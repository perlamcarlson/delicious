from django.contrib import admin 
from django.conf import settings
from django.db import models

# Create your models here.



class MealPlan(models.Model):
    name = models.CharField(max_length= 120)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meal_plans",
        on_delete=models.CASCADE
    )
    USER_MODEL = settings.AUTH_USER_MODEL
    date = models.DateField
    recipes = models.ManyToManyField(
        "recipes.Recipe",
        related_name= "meal_plans"
    )