from django.urls import path 
from meal_plans.views import (
     MealPlanListView, MealPlanDeleteView, MealPlanUpdateView, MealPlanCreateView,
     MealPlanDetailView,
)


urlpatterns=[
    path("", MealPlanListView.as_view(), name="mealplans_list"),
    path("meal_plans/<int:pk>/", MealPlanDetailView.as_view(), name="mealplans_detail"),
    path("meal_plans/<int:pk>/delete",MealPlanDeleteView.as_view(), name="mealplans_delete"),
    path("new/", MealPlanCreateView.as_view(), name="mealplans_new"),
    path("meal_plans/<int:pk>/edit", MealPlanUpdateView.as_view(), name="mealplans_edit"),
   
    

]
